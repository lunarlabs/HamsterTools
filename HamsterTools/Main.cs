﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;

namespace HamsterTools
{
    class Kickstart
    {
        
        static void Main(string[] args)
        {
            Debug.WriteLine("HamsterTools, " + OHRRPGCE.CompatibleVersion + "-compatible");
            Debug.WriteLine("Written by Matthias Lamers 2014-2015");
            Debug.WriteLine("OHRRPGCE lump file version " + OHRRPGCE.CurrentRPGVersion);
            Debug.WriteLine("RSAV version " + OHRRPGCE.CurrentRSAVVersion);
            Debug.WriteLine("");

            string thefile = @"C:\OHRRPGCE\vikings.rpg";
            LumpManager.unLump(thefile);

            //if we're debugging don't call the reaper
            if (!AppDomain.CurrentDomain.FriendlyName.EndsWith("vshost.exe"))
            {
                Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
                Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ReloadSpy());
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs t)
        {
            throw new NotImplementedException();
        }

        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs t)
        {
            DialogResult result = DialogResult.Cancel;
            try
            {
                result = CallGrimReaper(true, t.Exception);
            }
            catch
            {
                try
                {
                    MessageBox.Show("Sorry, a fatal error occured.", "HamsterTools",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                finally
                {
                    Application.Exit();
                }
            }
        }

        private static DialogResult CallGrimReaper(Boolean recoverable, Exception e)
        {
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            if (recoverable)
            {
                buttons = MessageBoxButtons.AbortRetryIgnore;
            }
            string errorMsg = "Sorry, a program error occured. Please contact Ichiro with the following information: \n\n";
            errorMsg = errorMsg + e.Message + "\n\nStack Trace:\n" + e.StackTrace;
            return MessageBox.Show(errorMsg, "The Grim Reaper", buttons, MessageBoxIcon.Stop);
        }
    }
}
